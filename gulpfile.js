'use strict';

var gulp = require('gulp');
var autoprefixer = require('gulp-autoprefixer');
var sass = require('gulp-sass');
var browserify = require('gulp-browserify');
var rename = require('gulp-rename');
var uglify = require('gulp-uglify');

gulp.task('js', function () {
    gulp.src('public/assets/js/main.js')
        .pipe(browserify({ debug: true }))
        .pipe(rename('public/assets/js/dist/bundle.js'))
        .pipe(gulp.dest('./'))
        .pipe(uglify())
        .pipe(rename('public/assets/js/dist/bundle.min.js'))
        .pipe(gulp.dest('./'))
    ;
});

gulp.task('js:watch', function () {
    gulp.watch('public/assets/js/*.js', ['js']);
})

gulp.task('sass', function () {
    return gulp.src('public/assets/scss/**/*.scss')
        .pipe(sass({outputStyle: 'compressed'}))
        .pipe(autoprefixer({
            browsers: ['> 1%', 'IE 8']
        }))
        .pipe(gulp.dest('public/assets/css'))
        ;
})

gulp.task('sass:watch', function () {
    gulp.watch('public/assets/scss/*.scss', ['sass']);
})

gulp.task('default', ['sass:watch', 'js:watch']);
